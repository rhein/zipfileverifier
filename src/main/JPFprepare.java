package main;


import java.io.File;
import java.io.IOException;

/** Prepares the generated code for Compilation (adds the TestEnvironment).
 *  Prepares the configuration used by JPF before the Verification starts.
 * 	This is for verification without VariabilityEncoding.
 * @author rhein
 */
public class JPFprepare {
	
	File toolsDir;
	{
		toolsDir = new File(CheckZipFile.thisDir, "tools");
		if (!toolsDir.exists()) {
			throw new InternalError("Did not find the tools directory");
		}
	}
	
	public void generateJPFconfigFile(File configFile, File testeeClasspath) throws IOException {
		String sep = File.separator;
		String toolsdirPath = toolsDir.getAbsolutePath();
		String testeePath = testeeClasspath.getAbsolutePath();
		if (File.separatorChar=='\\') {
			// windows OS
			sep = "\\\\"; //jpf will read these config files again and therefore the seperators must be double escaped.
			toolsdirPath = toolsdirPath.replace("\\", "\\\\");
			testeePath = testeePath.replace("\\", "\\\\");
		}
		String defaultConfig = 
				// this is for jpf-core
				"@include " + toolsdirPath + sep + "jpf.properties\n" +
						
				"target = Test_Runner\n" +
				"cg.enumerate_random=true\n" +
				"vm.enumerate_random=true\n" +
				"vm.por = true\n" +
				"vm.por.field_boundaries = true\n";
		
		String additionalProperties ="";
		File additionalConfig = new File(toolsdirPath+sep+".."+sep,"additionalPRODUCTconfig.properties");
		if (additionalConfig.exists()) {
			additionalProperties = "\n@include " + additionalConfig.getAbsolutePath() + "\n";
		}
		
		Util.writeFile(configFile, defaultConfig);
		String replacement = "classpath+=;" + testeePath +"\n"
				 + "classpath+=;" + toolsdirPath + sep + "aspectjrt.jar" + "\n"
				 + "jpf-core.native_classpath+=;" + testeePath +"\n"
				 + "listener+=;JPF_PrimitiveLogListener\n"
				 + additionalProperties;
		Util.appendToFile(configFile, replacement);
	}
}
