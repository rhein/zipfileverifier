package main;
import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

public class CheckZipFile {

	public static class Configuration {
		public String productlines = null;
		public int specification=-100;
		public List<Integer> problemSizes = new LinkedList<Integer>();
		public boolean checkProducts=true;
		public boolean checkBDD=true;
		public boolean userConfiguredProblemSize = false;
		public boolean checkCoreDFS=true;
		public boolean checkCoreBFS=false;
		public String productline;
	}

	public static class VerificationResult {
		long time;
		long maxMemory; // in KB
		long compile_time;
		boolean defect;
		String result;
		String states;
		boolean product = false;
		String usedConfiguration = "NA";
		int problemSize;
		String featureDesc;

		public VerificationResult(long time, long maxMem, boolean b, String contents, String states) {
			this.time = time;
			this.maxMemory = maxMem;
			this.defect = b;
			this.result = contents;
			this.states = states;
		}
		private void setConfiguration(String usedConfiguration, Integer problemSize, String featureDescription) {
			this.problemSize = problemSize;
			this.usedConfiguration=usedConfiguration;
			this.featureDesc = featureDescription;
			product = !(usedConfiguration.equals("bdd") || usedConfiguration.equals("core"));
		}
		public void setCompile_time(long compile_time) {
			this.compile_time = compile_time;
		}
	}

	private static int RAMsize = 15000;

	public static File thisDir;

	public static void printHelp() {
		System.out.println("illegal number of arguments:");
		System.out.println("Usage: without arguments, everything is checked");
		System.out.println("Usage: with arguments:");
		System.out.println("java -jar ZipFileChecker.jar [-ramLimit <limit>] [-tasks <Productline> <Specification> <ProblemSize> <Tool>]");
		System.out.println("<limit> is the main memory size given to JPF in mb (only integer values)");
		System.out.println("<Productline> can be x (all zip files) or a list of zip files (comma separated, without .zip ending");
		System.out.println("<Specification> is one integer (the ID of the specification to be checked) or x. ");
		System.out.println("<ProblemSize> is one or several integers (comma seperated) (the size of the problem to be checked) or x. ");
		System.out.println("<Tool> is be one of x, BDD, CoreDFS, CoreBFS or products or any combination seperated with ',' (no blanks)");
	}
	/**
	 * @param args
	 * @throws IOException 
	 * @throws VerificationFrameworkException 
	 */
	public static void main(String[] args) throws IOException, Exception {
		{
			File dir = new File("");
			thisDir = dir.getAbsoluteFile();
			System.out.println("The jar runs in: " + thisDir.getAbsolutePath());
		}

		File tmpDir = new File(thisDir, "tmp/");
		File resultsDir = new File (thisDir, "results/");

		Configuration config = new Configuration();
		for (int argProcessingIndex = 0; argProcessingIndex< args.length; argProcessingIndex++) {
			if ("-ramLimit".equals(args[argProcessingIndex])) {
				argProcessingIndex++;
				RAMsize=Integer.parseInt(args[argProcessingIndex]);
			} else if("-tasks".equals(args[argProcessingIndex])) {
				argProcessingIndex++;
				System.out.println("parsing user configuration");
				if (args[argProcessingIndex].equals("x")) {
					System.out.println(" -- Any productline");
				} else {
					config.productlines = args[argProcessingIndex];
					System.out.println(" -- Only " + config.productlines);
				}
				argProcessingIndex++;
				if (args[argProcessingIndex].equals("x")) {
					System.out.println(" -- All specs");
				} else {
					config.specification = Integer.parseInt(args[argProcessingIndex]);
					System.out.println(" -- Only spec " + config.specification);
				}
				argProcessingIndex++;
				if (args[argProcessingIndex].equals("x")) {
					System.out.println(" -- All problem sizes");
				} else {
					config.problemSizes.clear();

					String[] pSizes = args[argProcessingIndex].split(",");
					String strSizes ="";
					for (String size : pSizes) {
						int iSize = Integer.parseInt(size);
						config.problemSizes.add(iSize);
						strSizes +=(config.problemSizes.size()>1?",":"") + iSize;
					}
					config.userConfiguredProblemSize = true;
					System.out.println(" -- Only problem sizes " + strSizes);
				}
				argProcessingIndex++;
				if (args[argProcessingIndex].equals("x")) {
					System.out.println(" -- All available tools (BDD, Core, Products)");
				}else if (args[argProcessingIndex].contains(",")) {
					// multiple tools
					config.checkCoreDFS = config.checkCoreBFS = config.checkProducts = config.checkBDD = false;
					String[] tools = args[argProcessingIndex].split(",");
					for (String tool : tools)
						if (tool.toUpperCase().equals("BDD")) {
							config.checkBDD=true;
							System.out.println(" -- checking BDD");
						} else if (tool.toUpperCase().equals("COREDFS")) {
							config.checkCoreDFS=true;
							System.out.println(" -- checking CORE(DFS)");
						} else if (tool.toUpperCase().equals("COREBFS")) {
							config.checkCoreBFS=true;
							System.out.println(" -- checking CORE(BFS)");
						} else if (tool.toUpperCase().equals("PRODUCTS")) {
							config.checkProducts=true;
							System.out.println(" -- checking Products");
						} else throw new IllegalArgumentException("unrecognized tool : " + tool);
				} else if (args[argProcessingIndex].toUpperCase().equals("BDD")){
					config.checkBDD = true;
					config.checkCoreDFS = config.checkCoreBFS = config.checkProducts = false;
					System.out.println(" -- checking only with BDD");
				} else if (args[argProcessingIndex].toUpperCase().equals("COREDFS")) {
					config.checkCoreDFS = true;
					config.checkBDD = config.checkProducts = config.checkCoreBFS = false;
					System.out.println(" -- checking only with Core(DFS)");
				} else if (args[argProcessingIndex].toUpperCase().equals("COREBFS")) {
					config.checkCoreBFS = true;
					config.checkBDD = config.checkProducts = config.checkCoreDFS = false;
					System.out.println(" -- checking only with Core(BFS)");
				} else if (args[argProcessingIndex].toUpperCase().equals("PRODUCTS")){
					config.checkProducts = true;
					config.checkBDD = config.checkCoreDFS = config.checkCoreBFS = false;
					System.out.println(" -- checking only the products");
				}
			} else {
				printHelp();
				System.exit(0);

			}
		}

		System.out.println("Using " + RAMsize + "mb for JPF");
		if (config.productlines!= null && config.productlines.equals("x")) {
			String[] zipFiles = thisDir.list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".zip");
				}
			});
			for (String filename : zipFiles) {
				File zipFile = new File(thisDir, filename);
				checkZipFile(tmpDir, resultsDir, zipFile, config);
			}
		} 
		String[] tools = config.productlines.split(",");
		for (int i = 0; i < tools.length; i++) {
			File zipFile = new File(thisDir, tools[i] + ".zip");
			if (zipFile.exists())
				checkZipFile(tmpDir, resultsDir, zipFile, config);
			else 
				System.out.println(zipFile.getName() + " not found");
		}
	}

	private static List<VerificationResult> checkZipFile(File tmpDir, File resultsDir, File zipFile, Configuration config)
			throws IOException, Exception {
		System.out.println("start zipfilecheck");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("h:mm dd.MM.yy");
		File mainResultFile = new File(resultsDir, zipFile.getName() + ".csv");
		File resultsSummaryFile = new File(resultsDir, zipFile.getName() + "_summary.txt");
		Util.writeFile(resultsSummaryFile, "Results of the verification of ZIP file " + zipFile + " " + sdf.format(cal.getTime()) + "\n");
		List<VerificationResult> productlineResults = new LinkedList<CheckZipFile.VerificationResult>();
		Util.writeFile(mainResultFile, "Results of the verification of " + zipFile.getName() 
				+ "\nspec\tscalingFactor\tproduct\tdefect\ttime\tmemory\tstates\tcompileTime\tfeatures\tdescription\n");
		File tmpSrcDir = new File(tmpDir, "src");
		File tmpBinDir = new File(tmpDir, "bin");
		File tmpJPFLogDir = new File(tmpDir, "log");
		System.out.println("start getsubdirs");
		TreeSet<String> specs = Util.getZipFileSubdirs(zipFile.getAbsolutePath(), "");

		for (String spec: specs) {
			System.out.println("for spec "+ spec);
			int specID = 0;
			for (int i = 0; i < spec.length(); i++) {
				if (Character.isDigit(spec.charAt(i))) {
					specID = Integer.parseInt(spec.substring(i));
					break;
				}
			}
			if (config.specification!=-100 && config.specification!= specID) {
				System.out.println("skipping " + zipFile.getName() + " spec " + specID);
			} else {
				TreeSet<String> products = Util.getZipFileSubdirs(zipFile.getAbsolutePath(), spec+"/");
				LinkedList<VerificationResult> spec_results = new LinkedList<CheckZipFile.VerificationResult>();
				for (String product : products) {
					boolean checkThisProduct = true;
					if (product.equals("productSimulatorBDD")) {
						if (!config.checkBDD) {
							System.out.println("skipping product " + product);
							checkThisProduct=false;
						}
					} else if (product.equals("productSimulatorCore")) {
						if (!config.checkCoreDFS && !config.checkCoreBFS) {
							System.out.println("skipping product " + product);
							checkThisProduct=false;
						}
					} else {
						if (!config.checkProducts) {
							System.out.println("skipping product " + product);
							checkThisProduct=false;
						}
					}
					if (checkThisProduct) {
						String productShort = "";
						if (product.equals("productSimulatorBDD"))
							productShort="psBDD";
						else if (product.equals("productSimulatorCore")) {
							productShort="psCore";
						} else {
							for (int i = 0; i < product.length(); i++) {
								if (Character.isDigit(product.charAt(i))) {
									productShort = product.substring(i);
									break;
								}
							}
						}

						Util.deleteDirContents(tmpSrcDir);
						Util.deleteDirContents(tmpBinDir);
						// unzip						
						Util.unzip(zipFile.getAbsolutePath(), tmpSrcDir.getAbsolutePath(), spec +"/" + product + "/");

						// read feature information

						String[] usedFeatures = {"simulator"};
						if (! (product.equals("productSimulatorCore") || product.equals("productSimulatorBDD"))) {
							usedFeatures = Util.readFile(new File(tmpSrcDir, "usedFeatures.txt")).split("\n");
						}
						// compile
						System.out.println("compiling " + zipFile + " : " + spec + " / " + product);
						long compileTime = 0;
						if (product.equals("productSimulatorCore")) {
							List<String> classPathElements = new ArrayList<String>();
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "aspectjrt.jar");
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "build"+ File.separator + "jpf.jar");
							compileTime=compileWithAspectJ(tmpDir, tmpSrcDir, tmpBinDir, classPathElements);
						} else if (product.equals("productSimulatorBDD")) {
							List<String> classPathElements = new ArrayList<String>();
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "aspectjrt.jar");
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "build"+ File.separator + "jpf.jar");
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "jpf-bdd.jar");
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "jpf-bdd-classes.jar");
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "jpf-bdd-annotations.jar");
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "javabdd.jar");
							compileTime=compileWithAspectJ(tmpDir, tmpSrcDir, tmpBinDir, classPathElements);
						} else {
							List<String> classPathElements = new ArrayList<String>();
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "aspectjrt.jar");
							classPathElements.add(thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "build"+ File.separator + "jpf.jar");
							compileTime=compileWithAspectJ(tmpDir, tmpSrcDir, tmpBinDir, classPathElements);
						}

						// check
						for (Integer problemSize : config.problemSizes) {
							System.out.println("checking " + zipFile + " : " + spec + " / " + product + " with problemSize " + problemSize);
							if (product.equals("productSimulatorCore")) {
								JPFprepareLifting preparation = new JPFprepareLifting();
								File jpfConfig = new File(tmpDir, "jpfConfig_core.jpf");
								preparation.generateJPFconfigFile(jpfConfig, tmpBinDir);
								if (config.checkCoreDFS) {
									VerificationResult result;
									try {
										result = runCheck(specID + "_" + problemSize, jpfConfig, tmpJPFLogDir, false);
									} catch (Exception e) {
										e.printStackTrace();
										continue; // with next task
									}
									result.setCompile_time(compileTime);
									result.setConfiguration("coreDFS",problemSize,"simulator");
									Util.appendToFile(mainResultFile, specID+"\t"+problemSize+"\t"+productShort+"DFS"+"\t"+result.defect+"\t"+result.time+"\t"+result.maxMemory+"\t"+result.states+"\t"+result.compile_time+"\t"+result.featureDesc+"\t"+result.result+"\n");
									spec_results.add(result);
								}
								if (config.checkCoreBFS) {
									VerificationResult result;
									try {
										result = runCheck(specID + "_" + problemSize, jpfConfig, tmpJPFLogDir, true);
									} catch (Exception e) {
										e.printStackTrace();
										continue; // with next task
									}
									result.setCompile_time(compileTime);
									result.setConfiguration("coreBFS",problemSize,"simulator");
									Util.appendToFile(mainResultFile, specID+"\t"+problemSize+"\t"+productShort+"BFS"+"\t"+result.defect+"\t"+result.time+"\t"+result.maxMemory+"\t"+result.states+"\t"+result.compile_time+"\t"+result.featureDesc+"\t"+result.result+"\n");
									spec_results.add(result);
								}
							} else if (product.equals("productSimulatorBDD")) {
								JPFprepareLifting_jpfBDD preparation = new JPFprepareLifting_jpfBDD();
								File jpfConfig = new File(tmpDir, "jpfConfig_bdd.jpf");
								preparation.generateJPFconfigFile(jpfConfig, tmpBinDir);
								VerificationResult result;
								try {
									result = runCheck(specID + "_" + problemSize, jpfConfig, tmpJPFLogDir, false);
								} catch (Exception e) {
									e.printStackTrace();
									continue; // with next task
								}
								result.setCompile_time(compileTime);
								result.setConfiguration("bdd",problemSize,"simulator");
								Util.appendToFile(mainResultFile, specID+"\t"+problemSize+"\t"+productShort+"\t"+result.defect+"\t"+result.time+"\t"+result.maxMemory+"\t"+result.states+"\t"+result.compile_time+"\t"+result.featureDesc+"\t"+result.result+"\n");
								spec_results.add(result);
							} else {
								File exceutionEngine = new File(tmpSrcDir, "ExecutionEngine.txt");
								//System.out.println("ExecutionEngine file: " + Util.readFile(exceutionEngine));
								if (exceutionEngine.exists() && Util.readFile(exceutionEngine).startsWith("java")) {
									VerificationResult result;
									try {
										result = runCheckWithPlainJava(specID + "_" + problemSize, tmpBinDir);
									} catch (Exception e) {
										e.printStackTrace();
										continue; // with next task
									}
									result.setConfiguration(productShort, problemSize, Util.funcZip(usedFeatures, ":"));
									result.setCompile_time(compileTime);
									Util.appendToFile(mainResultFile, specID+"\t"+problemSize+"\t"+productShort+"\t"+result.defect+"\t"+result.time+"\t"+result.maxMemory+"\t"+result.states+"\t"+result.compile_time+"\t"+result.featureDesc+"\t"+result.result+"\n");
									spec_results.add(result);
								} else {
									JPFprepare preparation = new JPFprepare();
									File jpfConfig = new File(tmpDir, "jpfConfig_product.jpf");
									preparation.generateJPFconfigFile(jpfConfig, tmpBinDir);
									VerificationResult result;
									try {
										result = runCheck(specID + "_" + problemSize, jpfConfig, tmpJPFLogDir, false);
									} catch (Exception e) {
										e.printStackTrace();
										continue; // with next task
									}
									result.setCompile_time(compileTime);
									result.setConfiguration(productShort, problemSize, Util.funcZip(usedFeatures, ":"));
									Util.appendToFile(mainResultFile, specID+"\t"+problemSize+"\t"+productShort+"\t"+result.defect+"\t"+result.time+"\t"+result.maxMemory+"\t"+result.states+"\t"+result.compile_time+"\t"+result.featureDesc+"\t"+result.result+"\n");
									spec_results.add(result);
								}
							}
							compileTime=0; // for the next tasks, we reuse the compiled system (only bill the compileTime once)
						}
					}
				}
				for (Integer problemSize : config.problemSizes) {
					SummaryGeneration.writeSpecSummary(spec_results, specID, resultsSummaryFile, problemSize);
					SummaryGeneration.writeSpecSummary_SampleBased(spec_results, specID, resultsSummaryFile, new File(thisDir, "sampleSet_"+zipFile.getName() + ".txt"), problemSize);
				}
				productlineResults.addAll(spec_results);
			}
		}
		for (Integer problemSize : config.problemSizes) {
			SummaryGeneration.writeProductlineSummary(productlineResults, resultsSummaryFile, problemSize);
		}
		return productlineResults;
	}

	/**
	 * Compiles the given sources with AspectJ.
	 * @param sourceDir
	 * @param destinationDir
	 * @param destinationDir 
	 * @param classPathElements
	 * @throws IOException
	 * @throws VerificationFrameworkException
	 */
	static long compileWithAspectJ(File tmpDir, File sourceDir, File destinationDir, List<String> classPathElements) throws IOException, Exception {
		String prefix = "<?xml version=\"1.0\"?>\n" 
				+ "<project name=\"AspectJLauncher\" default=\"compileWithAspectJ\">\n" 
				+ "  <target name=\"clean\" description=\"Delete all generated files\">\n" 
				+ "    <delete dir=\"${destinationClassFiles.dir}\" failonerror=\"false\"/>\n" 
				+ "  </target>\n";
		String postfix =  "<taskdef \n"
				+ "      resource=\"org/aspectj/tools/ant/taskdefs/aspectjTaskdefs.properties\">\n"
				+ "    <classpath>\n"
				+ "      <pathelement location=\"" 
				+ thisDir.getAbsolutePath() + File.separator +"tools" + File.separator + "aspectjtools.jar\"/>\n"
				+ "    </classpath>\n"
				+ "  </taskdef>\n"
				+ "\n"
				+ "  <target name=\"compileWithAspectJ\" depends=\"clean\">\n"
				+ "    <iajc\n"
				+ "		destDir=\"${destinationClassFiles.dir}\"\n"
				+ "		sourceroots=\"${sourceAspectJ.dir}\"\n"
				+ "		source=\"1.5\"\n"
				+ "    	failonError=\"true\"\n"
				+ "    	showWeaveInfo=\"true\"\n"
				+ "    	verbose=\"true\">\n"
				+ "        <aspectpath refid=\"aspectClassPaths.path\"/>\n"
				+ "	</iajc>\n"
				+ "  </target>\n"
				+ "</project>\n";


		// assemble classpath entries
		String classpath = "";
		classpath = classpath + "<property name=\"destinationClassFiles.dir\" value=\"" + destinationDir.getAbsolutePath() + "\" />\n";
		classpath = classpath + "<property name=\"sourceAspectJ.dir\" value=\"" + sourceDir.getAbsolutePath() + "\" />\n";
		classpath = classpath + "<path id=\"aspectClassPaths.path\">\n";
		for (String pathElement : classPathElements) {
			classpath = classpath + "<pathelement path=\""+pathElement + "\"/>\n";
		}
		classpath = classpath + "</path>\n";
		// copy the antScript and insert the classpath entries

		File antScriptTmp = new File(tmpDir, "antScriptTmp.xml");
		Util.writeFile(antScriptTmp, prefix + classpath + postfix);

		Project p = new Project();
		p.setUserProperty("ant.file", antScriptTmp.getAbsolutePath());
		p.init();
		ProjectHelper.configureProject(p, antScriptTmp);
		// execute the copy script
		if (!Util.deleteDirContents(destinationDir))
			throw new Exception("Could not clean AspectJ OutputDirectory");
		try {
			long startTime = System.currentTimeMillis();
			p.executeTarget("compileWithAspectJ");
			System.out.println("AspectJ compiled without error");
			return System.currentTimeMillis() - startTime;
		} catch (BuildException e) {
			throw new Exception("AspectJ Task completed with Error:" + e.getMessage(), e);
		}
		// delete the copy script
		//antScriptTmp.delete();
	}

	public static VerificationResult runCheck(String testeeArguments, File configFile, File tmpJPFLogDir, boolean forceBFS) throws Exception {
		try {
			File siteFile = new File(thisDir, File.separator + "tools"+ File.separator + "site.properties");
			// initialize site.contents
			String siteContents = "jpf-core=" + thisDir + File.separator + "tools\n";
			if (! siteFile.exists() || ! Util.readFile(siteFile).equals(siteContents)) {
				Util.writeFile(siteFile, siteContents);
			}
			final Process p;
			String 	commandline = "/usr/bin/time -f JavaSPLVerTimeSummary:SystemTime:%S,UserTime:%U,MaxMemory:%M\n " + "java "
					+ "-Xmx" + RAMsize + "m -Xms" + RAMsize + "m -ea -jar " 
					+ thisDir.getAbsolutePath() + File.separator + "tools"+ File.separator + "RunJPF.jar +site=" + siteFile.getAbsolutePath();

			if (testeeArguments != null && testeeArguments.length() > 0) {
				System.out.println("runJPF:" + testeeArguments);

				commandline += " +target.args=" + testeeArguments;
			}
			if (forceBFS)
				commandline += " +search.class=gov.nasa.jpf.search.heuristic.BFSHeuristic";
			commandline += " " + configFile.getAbsolutePath();
			System.out.println(commandline);

			p = Runtime.getRuntime().exec(commandline, new String[]{""});
			//p = Runtime.getRuntime().exec(commandline, null);


			StreamReaderThread out = new StreamReaderThread(p.getInputStream(), System.out);
			Thread readerOut = new Thread(out);
			StreamReaderThread err = new StreamReaderThread(p.getErrorStream(), System.err);
			Thread readerErr = new Thread(err);

			readerOut.start();
			readerErr.start();
			p.waitFor();
			readerOut.join(0);
			readerErr.join(0);

			System.out.println("Measured cputime: " + err.time + " ms");
			if (out.listenerResult == null) {
				throw new Exception("No result parsed!");
			} else {
				if (out.listenerResult.trim().toLowerCase().startsWith("succeeded")) {
					return new VerificationResult(err.time,err.memory, false, out.listenerResult, out.listenerStatesMessage);
				} else {
					return new VerificationResult(err.time,err.memory, true, out.listenerResult, out.listenerStatesMessage);
				}
			}
		} catch (IOException e) {
			System.out.println("IOException with arguments " + testeeArguments);
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("InterruptedEx with arguments " + testeeArguments);
			e.printStackTrace();
		}
		throw new Exception("NO verification executed?");
	}

	public static VerificationResult runCheckWithPlainJava(String testeeArguments, File testeeClassPath) throws Exception {
		try {
			// initialize site.contents
			final Process p;
			String 	commandline = "/usr/bin/time -f JavaSPLVerTimeSummary:SystemTime:%S,UserTime:%U,MaxMemory:%M\n " + "java "
					+ "-Xmx" + RAMsize + "m -Xms" + RAMsize + "m -ea -cp " + testeeClassPath.getAbsolutePath() + " Test_Runner ";

			if (testeeArguments != null && testeeArguments.length() > 0) {
				System.out.println("testeeArguments:" + testeeArguments);

				commandline += " " + testeeArguments;
			}

			System.out.println(commandline);

			p = Runtime.getRuntime().exec(commandline, new String[]{""});
			//p = Runtime.getRuntime().exec(commandline, null);

			StreamReaderThread out = new StreamReaderThread(p.getInputStream(), System.out);
			Thread readerOut = new Thread(out);
			StreamReaderThread err = new StreamReaderThread(p.getErrorStream(), System.err);
			Thread readerErr = new Thread(err);

			readerOut.start();
			readerErr.start();
			p.waitFor();
			readerOut.join(0);
			readerErr.join(0);

			System.out.println("Measured cputime: " + err.time + " ms");
			if (out.listenerResult == null) {
				throw new Exception("No result parsed!");
			} else {
				if (out.listenerResult.trim().toLowerCase().startsWith("succeeded")) {
					return new VerificationResult(err.time, err.memory, false, out.listenerResult, out.listenerStatesMessage);
				} else {
					return new VerificationResult(err.time, err.memory, true, out.listenerResult, out.listenerStatesMessage);
				}
			}
		} catch (IOException e) {
			System.out.println("IOException with arguments " + testeeArguments);
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("InterruptedEx with arguments " + testeeArguments);
			e.printStackTrace();
		}
		throw new Exception("NO verification executed?");
	}


	static class StreamReaderThread implements Runnable {
		InputStream is;
		PrintStream ps;

		String listenerResult = null;
		String listenerStatesMessage = null;
		long time = 0;
		long memory = 0;
		private String resultMessagePrefix="jpfResultMessagePrefix: ";
		private String statesMessagePrefix="jpfStatesMessagePrefix: ";

		public StreamReaderThread(InputStream is, PrintStream ps) {
			this.is = is;
			this.ps = ps;
		}
		@Override
		public void run() {
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = null;
				while ((line = br.readLine()) != null) {
					if (!line.startsWith("[WARNING] annotation type not found: org.aspectj."))
						ps.println(line);
					if (line.startsWith(resultMessagePrefix)) {
						listenerResult = line.substring(resultMessagePrefix.length());
					} else if (line.startsWith(statesMessagePrefix)) {
						listenerStatesMessage = line.substring(statesMessagePrefix.length());
					} else if (line.startsWith("error") && line.contains("gov.nasa.jpf.jvm.NoOutOfMemoryErrorProperty")) {
						listenerResult = "Out Of Memory";
						listenerStatesMessage = "NA";
					} else if (line.startsWith("JavaSPLVerTimeSummary:")) {
						//JavaSPLVerTimeSummary: SytemTime:%S,UserTime:%U
						String[] components = line.substring("JavaSPLVerTimeSummary:".length()).split(",");
						double systime = Double.parseDouble(components[0].substring("SystemTime:".length()));
						double usrtime = Double.parseDouble(components[1].substring("UserTime:".length()));
						memory = Long.parseLong(components[2].substring("MaxMemory:".length()));
						time = (long) (systime*1000) + (long) (usrtime*1000);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}