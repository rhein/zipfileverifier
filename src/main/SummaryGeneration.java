package main;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


public class SummaryGeneration {

	private static class SummaryStats {
		int timeSum;
		int productsChecked;
		int defectsFound;
	}

	static void writeSpecSummary_SampleBased(
			LinkedList<CheckZipFile.VerificationResult> spec_results, int specID, File summaryFile, File sampleSet, int filterProblemSize) {
		// extract products from sample set file
		HashSet<String> sampleProducts = new HashSet<String>();
		try {
			if (!sampleSet.exists()) {
				Util.appendToFile(summaryFile, "sampleSetFile (" + sampleSet.getName() + ") not found.\n");
				return;
			}
			String[] fileLines = Util.readFile(sampleSet).split("\n");
			for (String line: fileLines) {
				if (!line.trim().equals("")) {
					sampleProducts.add(line.split(" ")[0].substring("product".length())); // add the part of the line before the first space without "product" prefix.
				}
			}
		} catch (FileNotFoundException e1) {
		} catch (IOException e) {
		}
		
		StringBuilder summary = new StringBuilder();
		SummaryStats productBased = new SummaryStats();
		for (CheckZipFile.VerificationResult res : spec_results) {
			if (res.problemSize==filterProblemSize) {
				if (sampleProducts.contains(res.usedConfiguration)) {
					if (res.product) {
						productBased.productsChecked++;
						if (res.defect) productBased.defectsFound++;
						productBased.timeSum+=res.time;
					}
				}
			}
		}
		summary.append("Spec" + specID + " scalingFactor" + filterProblemSize + " sample-Based: totalTime:" + productBased.timeSum + " defects: " + productBased.defectsFound + " totalProducts: " + productBased.productsChecked + "\n");
		try {
			Util.appendToFile(summaryFile, summary.toString());
		} catch (IOException e) {
			System.err.println("Could not write to summary File: " + e.getMessage());
			e.printStackTrace();
		}
	}

	static void writeSpecSummary(
			LinkedList<CheckZipFile.VerificationResult> spec_results, int specID, File summaryFile, int filterProblemSize) {
		StringBuilder summary = new StringBuilder();
		SummaryStats productBased = new SummaryStats();
		for (CheckZipFile.VerificationResult res : spec_results) {
			if (res.problemSize==filterProblemSize) {
				if (res.product) {
					productBased.productsChecked++;
					if (res.defect) productBased.defectsFound++;
					productBased.timeSum+=res.time;
				} else if (res.usedConfiguration.equals("bdd")) {
					summary.append("Spec" + specID + " scalingFactor" + filterProblemSize + " jpf-bdd: " + res.time + " defect: " + res.defect + "\n");
				} else if (res.usedConfiguration.equals("core")) {
					summary.append("Spec" + specID + " scalingFactor" + filterProblemSize + " jpf-core: " + res.time + " defect: " + res.defect + "\n");
				}
			}
		}
		summary.append("Spec" + specID + " scalingFactor" + filterProblemSize + " products: totalTime:" + productBased.timeSum + " defects: " + productBased.defectsFound + " totalProducts: " + productBased.productsChecked + "\n");
		try {
			Util.appendToFile(summaryFile, summary.toString());
		} catch (IOException e) {
			System.err.println("Could not write to summary File: " + e.getMessage());
			e.printStackTrace();
		}
	}

	static void writeProductlineSummary(
			List<CheckZipFile.VerificationResult> productlineResults, File resultsSummaryFile, int filterProblemSize) {
		StringBuilder summary = new StringBuilder();
		SummaryStats productBased = new SummaryStats();
		SummaryStats familyBDD = new SummaryStats();
		SummaryStats familyCore = new SummaryStats();
		for (CheckZipFile.VerificationResult res : productlineResults) {
			if (res.problemSize==filterProblemSize) {
				if (res.product) {
					productBased.productsChecked++;
					if (res.defect) productBased.defectsFound++;
					productBased.timeSum+=res.time;
				} else if (res.usedConfiguration.equals("bdd")) {
					familyBDD.productsChecked++;
					if (res.defect) familyBDD.defectsFound++;
					familyBDD.timeSum+=res.time;
				} else if (res.usedConfiguration.equals("core")) {
					familyCore.productsChecked++;
					if (res.defect) familyCore.defectsFound++;
					familyCore.timeSum+=res.time;
				}
			}
		}
		summary.append("\n--Total results for the productline (sum over all specifications, scalingFactor" + filterProblemSize + ")---\n");
		if (familyBDD.productsChecked > 0)
			summary.append("family with jpf-bdd: timeSum: " + familyBDD.timeSum + "\t defectsFound: " + familyBDD.defectsFound + "\t productsChecked: " + familyBDD.productsChecked);
		if (familyCore.productsChecked > 0)
			summary.append("family with jpf-core: timeSum: " + familyCore.timeSum + "\t defectsFound: " + familyCore.defectsFound + "\t productsChecked: " + familyCore.productsChecked);
		if (productBased.productsChecked > 0)
			summary.append("product-based       : timeSum: " + productBased.timeSum + "\t defectsFound: " + productBased.defectsFound + "\t productsChecked: " + productBased.productsChecked);
		try {
			Util.appendToFile(resultsSummaryFile, summary.toString() + "\n");
		} catch (IOException e) {
			System.err.println("Could not write to summary File: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
